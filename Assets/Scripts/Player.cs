﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Player : PhysicsCollision
{
    [Header("Properties")]
    public float speed;
    float horizontalAxis;
    float normalizedMovement;
    bool jump;
    public float jumpForce;
    
   
 
    protected override void Start()
    {
        base.Start();
        
    }
    void Update()
    {
        
        if(Input.GetButton("Fire3")){
            
            Run();
        }else{
            HorizontalMovement();
        }
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
 
        if(jump)
        {
            rb.velocity = new Vector3(rb.velocity.x, 0, 0);
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            jump = false;
        }
 
        rb.velocity = new Vector3(normalizedMovement, rb.velocity.y, 0);
    }
 
 
    public void SetAxis(float h)
    {
        horizontalAxis = h;
    }
    public void JumpStart()
    {
        if(isGrounded) jump = true;
    }
 
    void HorizontalMovement()
    {
        
        if(horizontalAxis > 0 && !isFacingRight) Flip();
        else if(horizontalAxis < 0 && isFacingRight) Flip();
 
        if(isTouchingWall)
        {
            normalizedMovement = 0;
            return;
        }
 
        normalizedMovement = horizontalAxis * speed;
    }

    void Run(){
        normalizedMovement = horizontalAxis * speed*2;
        //if (Input.GetButtonDown(KeyCode.space)){
          //rb.AddForce(Vector3.up * jumpForce*2, ForceMode.Impulse);
            //jump = false;  
        //}
    }

    
    
 
}